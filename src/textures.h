#ifndef TEXTURES_H
#define TEXTURES_H

unsigned int num_0_tex;         
unsigned int num_1_tex;         
unsigned int num_2_tex;         
unsigned int num_3_tex;         
unsigned int num_4_tex;        
unsigned int num_5_tex;         
unsigned int num_6_tex;         
unsigned int num_7_tex;         
unsigned int num_8_tex;         
unsigned int num_9_tex;         
unsigned int g_scores_tex[4];   
unsigned int one_up_tex;        
unsigned int score_tex;        
unsigned int ready_tex;        
unsigned int gameover_tex;     
unsigned int help_tex;          
unsigned int quit_tex;          
unsigned int life_tex;          
unsigned int pause_tex;        
unsigned int pause_alt_tex;  

// Pac-Man
unsigned int pac_0_tex;
unsigned int pac_1_tex;
unsigned int pac_2_tex;

unsigned int map_tex;
unsigned int pill_tex;
unsigned int bigPill_tex[2];
// Pac-Man Death Textures
unsigned int dead_tex[11];      
// Ghost Textures
unsigned int ghost_r_tex[2];
unsigned int ghost_p_tex[2];    
unsigned int ghost_b_tex[2];
unsigned int ghost_y_tex[2];
unsigned int ghost_f_tex[4];    
// Eye Textures
unsigned int eye_u_tex;         
unsigned int eye_r_tex;        
unsigned int eye_d_tex;        
unsigned int eye_l_tex;         
// Fruit Textures
unsigned int fruits_tex[8];     
unsigned int f_score_tex[8];    

  

void loadBindTextures()
{
    // Enable blending, allowing transparency of PNG textures
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    map_tex =           load_exture("sprites/map/map.png");
    pill_tex =          load_exture("sprites/map/pill.png");
    bigPill_tex[0] =    load_exture("sprites/map/big-0.png");
    bigPill_tex[1] =    load_exture("sprites/map/big-1.png");
    // Bind Pac-Man textures
    pac_0_tex =         load_exture("sprites/pacman/0.png");
    pac_1_tex =         load_exture("sprites/pacman/1.png");
    pac_2_tex =         load_exture("sprites/pacman/2.png");
    // Bind Pac-Man Death textures
    dead_tex[0] =       load_exture("sprites/pacman/d-0.png");
    dead_tex[1] =       load_exture("sprites/pacman/d-1.png");
    dead_tex[2] =       load_exture("sprites/pacman/d-2.png");
    dead_tex[3] =       load_exture("sprites/pacman/d-3.png");
    dead_tex[4] =       load_exture("sprites/pacman/d-4.png");
    dead_tex[5] =       load_exture("sprites/pacman/d-5.png");
    dead_tex[6] =       load_exture("sprites/pacman/d-6.png");
    dead_tex[7] =       load_exture("sprites/pacman/d-7.png");
    dead_tex[8] =       load_exture("sprites/pacman/d-8.png");
    dead_tex[9] =       load_exture("sprites/pacman/d-9.png");
    dead_tex[10] =      load_exture("sprites/pacman/d-10.png");
    // Bind ghost textures
    ghost_r_tex[0] =    load_exture("sprites/ghosts/r-0.png");
    ghost_r_tex[1] =    load_exture("sprites/ghosts/r-1.png");
    ghost_p_tex[0] =    load_exture("sprites/ghosts/p-0.png");
    ghost_p_tex[1] =    load_exture("sprites/ghosts/p-1.png");
    ghost_b_tex[0] =    load_exture("sprites/ghosts/b-0.png");
    ghost_b_tex[1] =    load_exture("sprites/ghosts/b-1.png");
    ghost_y_tex[0] =    load_exture("sprites/ghosts/y-0.png");
    ghost_y_tex[1] =    load_exture("sprites/ghosts/y-1.png");
    ghost_f_tex[0] =    load_exture("sprites/ghosts/f-0.png");
    ghost_f_tex[1] =    load_exture("sprites/ghosts/f-1.png");
    ghost_f_tex[2] =    load_exture("sprites/ghosts/f-2.png");
    ghost_f_tex[3] =    load_exture("sprites/ghosts/f-3.png");
    // Bind ghost eye textures
    eye_u_tex =         load_exture("sprites/eyes/u.png");
    eye_r_tex =         load_exture("sprites/eyes/r.png");
    eye_d_tex =         load_exture("sprites/eyes/d.png");
    eye_l_tex =         load_exture("sprites/eyes/l.png");
    // Bind fruit textures
    fruits_tex[0] =     load_exture("sprites/fruits/cherry.png");
    fruits_tex[1] =     load_exture("sprites/fruits/strawberry.png");
    fruits_tex[2] =     load_exture("sprites/fruits/orange.png");
    fruits_tex[3] =     load_exture("sprites/fruits/apple.png");
    fruits_tex[4] =     load_exture("sprites/fruits/melon.png");
    fruits_tex[5] =     load_exture("sprites/fruits/boss.png");
    fruits_tex[6] =     load_exture("sprites/fruits/bell.png");
    fruits_tex[7] =     load_exture("sprites/fruits/key.png");
    f_score_tex[0] =    load_exture("sprites/ui/100.png");
    f_score_tex[1] =    load_exture("sprites/ui/300.png");
    f_score_tex[2] =    load_exture("sprites/ui/500.png");
    f_score_tex[3] =    load_exture("sprites/ui/700.png");
    f_score_tex[4] =    load_exture("sprites/ui/1000.png");
    f_score_tex[5] =    load_exture("sprites/ui/2000.png");
    f_score_tex[6] =    load_exture("sprites/ui/3000.png");
    f_score_tex[7] =    load_exture("sprites/ui/5000.png");
    // Bind UI textures
    num_0_tex =         load_exture("sprites/ui/0.png");
    num_1_tex =         load_exture("sprites/ui/1.png");
    num_2_tex =         load_exture("sprites/ui/2.png");
    num_3_tex =         load_exture("sprites/ui/3.png");
    num_4_tex =         load_exture("sprites/ui/4.png");
    num_5_tex =         load_exture("sprites/ui/5.png");
    num_6_tex =         load_exture("sprites/ui/6.png");
    num_7_tex =         load_exture("sprites/ui/7.png");
    num_8_tex =         load_exture("sprites/ui/8.png");
    num_9_tex =         load_exture("sprites/ui/9.png");
    g_scores_tex[0] =   load_exture("sprites/ui/200.png");
    g_scores_tex[1] =   load_exture("sprites/ui/400.png");
    g_scores_tex[2] =   load_exture("sprites/ui/800.png");
    g_scores_tex[3] =   load_exture("sprites/ui/1600.png");
    one_up_tex =        load_exture("sprites/ui/1up.png");
    score_tex =         load_exture("sprites/ui/score.png");
    ready_tex =         load_exture("sprites/ui/ready.png");
    gameover_tex =      load_exture("sprites/ui/gameover.png");
    help_tex =          load_exture("sprites/ui/help.png");
    quit_tex =          load_exture("sprites/ui/quit.png");
    life_tex =          load_exture("sprites/ui/life.png");
    pause_tex =         load_exture("sprites/ui/pause.png");
    pause_alt_tex =     load_exture("sprites/ui/pause_alt.png");
}


void rgb(float r, float g, float b)
{
    glColor3f(r / 255, g / 255, b / 255);
}


void drawSprite(unsigned int texture, int len, int h, float angle)
{
    int halflen = len / 2;
    int halfHeight = h / 2;
    glPushMatrix();
    rgb(255, 255, 255);
    glTranslatef((float)halflen,(float)halfHeight,0.0f);
    glRotatef(angle, 0.0f, 0.0f, 1.0f);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f);      // Bottom left
    glVertex2i(-halflen, -halfHeight);
    glTexCoord2f (1.0f, 0.0f);      // Bottom right
    glVertex2i(halflen, -halfHeight);
    glTexCoord2f (1.0f, 1.0f);      // Top right
    glVertex2i(halflen, halfHeight);
    glTexCoord2f (0.0f, 1.0f);      // Top left
    glVertex2i(-halflen, halfHeight);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

#endif /* !TEXTURES_H */
