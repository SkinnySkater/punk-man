#ifndef PACMAN_PACMAN_H
#define PACMAN_PACMAN_H

#define TUILE_SIZE 0.1f
#define TILE_SIZE 13
#define PACMAN_TILE_SIZE 15

extern int ticks;
extern int pillsLeft;
extern int fruits;
extern bool fruitSpawned;


class Pacman
{
private:
    float y;
    float x;
    float angle;
    int t_counter;
    direction dir;
    direction t_dir;
    direction s_dir;
    float dt_counter;
    bool ready;

public:
    Pacman()
    {
        x = 13.5f;
        y = 7.0f;
        angle = 0.0f;
        dir = NONE;
        t_dir = NONE;
        t_counter = 10;
        dt_counter = 0;
        ready = false;
    }

    void reset()
    {
        x = 13.5f;
        y = 7.0f;
        angle = 0.0f;
        dir = NONE;
        t_dir = NONE;
        t_counter = 10;
        dt_counter = 0;
        ready = false;
    }

    int getX()
    {
        return round(x);
    }


    int getY()
    {
        return round(y);
    }


    tile getNextTile(direction d)
    {
        // Return next tile in given direction
        switch(d)
        {
            case UP:
                return getTile(getX(), getY() + 1);
            case RIGHT:
                return getTile(getX() + 1, getY());
            case DOWN:
                return getTile(getX(), getY() - 1);
            case LEFT:
                return getTile(getX() - 1,getY());
            default:
                return getTile(getX(),getY());
        }
    }

    bool isAtCenter()
    {
        return (int)round(y * 10.0f) % 10 == 0 && (int)round(x * 10.0f) % 10 == 0;
    }

    direction getDirection()
    {
        return dir;
    }

    void setDirection(direction d)
    {
        t_dir = d;
    }

    void modeDirection(){
        switch(dir)
        {
            case UP:
                y += TUILE_SIZE;
                x = round(x);
                break;
            case RIGHT:
                x += TUILE_SIZE;
                y = round(y);
                break;
            case DOWN:
                y -= TUILE_SIZE;
                x = round(x);
                break;
            case LEFT:
                x -= TUILE_SIZE;
                y = round(y);
                break;
            default:             
                if (ready)
                {
                    x = round(x);
                    y = round(y);
                }
                break;
        }

    }

    void move()
    {
        if (isAtCenter())
        {
            if (!canPass(getNextTile(t_dir)))
                dir = t_dir;
            else if (canPass(getNextTile(dir)))
                dir = NONE;
        }
        if (!ready && t_dir != NONE && !canPass(getNextTile(t_dir)))
        {
            dir = t_dir;
            if (!ready)
                ready = true;
        }
        modeDirection();
    }


    int eatF(){
        setTile(getX(), getY(), e);
        fruitSpawned = false;
        switch(fruits++)
        {
            case 0:
                return 100;
            case 1:
                return 300;
            case 2:
                return 500;
            case 3:
                return 700;
            case 4:
                return 1000;
            case 5:
                return 2000;
            case 6:
                return 3000;
            case 7:
                return 5000;
        }

    }


    int eat()
    {
        if (isAtCenter())
        {
            switch(getTile(getX(), getY()))
            {
                case o:
                    setTile(getX(),getY(),e);
                    pillsLeft--;
                    return 10;
                case O:
                    setTile(getX(),getY(),E);
                    pillsLeft--;
                    return 50;
                case P:
                    if (dir == RIGHT)
                        x = 1;
                    else
                        x = 26;
                    return 0;
                case F:
                    eatF();
            }
        }
        return 0;
    }

    void drawDirection(){
        // Determine rotation angle of sprite based on direction in degree
        switch(dir)
        {
            case UP:
                angle = 270.0f;
                break;
            case RIGHT:
                angle = 180.0f;
                break;
            case DOWN:
                angle = 90.0f;
                break;
            case LEFT:
                angle = 0.0f;
                break;
        }
    }

    void draw()
    {
        unsigned int pacman_tex;
        glPushMatrix();
        translateMapOrigin();
        translateMapCoords(x,y);
        glTranslatef(-2.0f, -2.0f, 0.0f);
        drawDirection();
        if (t_counter % 20 < 5)
            pacman_tex = pac_0_tex;
        else if (t_counter % 20 < 10 || t_counter % 20 >= PACMAN_TILE_SIZE)
            pacman_tex = pac_1_tex;
        else
            pacman_tex = pac_2_tex;
        drawSprite(pacman_tex, TILE_SIZE, TILE_SIZE, angle);
        if (!(dir == NONE && t_counter % 20 < 5) && ready)
            t_counter++;
        glPopMatrix();
    }

    // sTOP Pac-Man's eating animation
    void stopFlashing()
    {
        s_dir = t_dir;
        t_dir = NONE;
        ready = false;
    }

    // Restart Pac-Man's eating animation
    void startFlashing()
    {
        if (t_dir == NONE)
            t_dir = s_dir;
        ready = true;
    }

    void drawDead()
    {
        glPushMatrix();
        translateMapOrigin();
        translateMapCoords(x, y);
        glTranslatef(-3.0f, -4.0f, 0.0f);
        int deadFrame = (int)floor(dt_counter / 5);
        unsigned int pacman_tex = dead_tex[deadFrame];
        if (dt_counter < 55)
            drawSprite(pacman_tex, PACMAN_TILE_SIZE, PACMAN_TILE_SIZE, 0);
        dt_counter++;
        glPopMatrix();
    }

    void drawFruitScore()
    {
        glPushMatrix();
        translateMapOrigin();
        translateMapCoords(x,y);
        glTranslatef(-6.0f, 0.0f, 0.0f);
        drawSprite(f_score_tex[fruits - 1], 20, 8, 0);
        glPopMatrix();
    }
};

Pacman pacman;

#endif /* !PACMAN_PACMAN_H */
