#ifndef PACMAN_GHOSTS_H
#define PACMAN_GHOSTS_H
#define FRAME_RATE 60
extern int ticks;
extern int ghostsEaten;
extern Pacman pacman;

movement wave = SCATTER;

class Ghost
{
private:
    float x; 
    float x_init;   
    float y;        
    float y_init;
    float d_pos;    
    color skin;   
    direction dir;
    int tex_count;  
    movement ai;    
    bool rev_flag;   
    int timeout;    
    bool display_score; 

public:
    Ghost(float x, float y, color c)
    {
        this->x = x;
        x_init = x;
        this->y = y;
        y_init = y;
        d_pos = 0.1f;
        skin = c;
        tex_count = 0;
        rev_flag = false;
        timeout = -1;
        display_score = false;
        initColor(skin);
    }

    void initColor(color skin){
        switch(skin)
        {
            case RED:
                dir = LEFT;
                ai = wave;
                break;
            case PINK:
                dir = DOWN;
                ai = LEAVE;
                break;
            case BLUE:
            case YELLOW:     
                dir = UP;
                ai = SPAWN;
                break;
        }
    }

    int getX()
    {
        return round(x);
    }

    int getY()
    {
        return round(y);
    }

    void reset()
    {
        x = x_init;
        y = y_init;
        d_pos = 0.1f;
        tex_count = 0;
        rev_flag = false;
        timeout = -1;
        display_score = false;
        initColor(skin);
    }


    tile getNextTile(direction d)
    {
        switch(d)
        {
            case UP:
                return getTile(getX(), getY() + 1);
            case RIGHT:
                return getTile(getX() + 1, getY());
            case DOWN:
                return getTile(getX(), getY() - 1);
            case LEFT:
                return getTile(getX() - 1, getY());
        }
    }

    bool isAtCenter()
    {
        int mod = 1 / d_pos;
        bool res = (int)round(y * mod) % mod == 0 
        && (int)round(mod * x) % mod == 0;
        return res;
    }


    int countExits()
    {
        int exits = 0;
        if (isAtCenter())
        {
            if (!canPass(getNextTile(UP)))
                exits++;
            if (!canPass(getNextTile(RIGHT)))
                exits++;
            if (!canPass(getNextTile(DOWN)))
                exits++;
            if (!canPass(getNextTile(LEFT)))
                exits++;
        }
        return exits;
    }


    void turnCorner()
    {
        if (dir != DOWN && !canPass(getNextTile(UP)))
            dir = UP;
        else if (dir != LEFT && !canPass(getNextTile(RIGHT)))
            dir = RIGHT;
        else if (dir != UP && !canPass(getNextTile(DOWN)))
            dir = DOWN;
        else if (dir != RIGHT && !canPass(getNextTile(LEFT)))
            dir = LEFT;
    }

    void rev_flagDirection()
    {
        switch(dir)
        {
            case UP:
                dir = DOWN;
                break;
            case RIGHT:
                dir = LEFT;
                break;
            case DOWN:
                dir = UP;
                break;
            case LEFT:
                dir = RIGHT;
                break;
        }
        rev_flag = false;
    }

    movement getAI()
    {
        return ai;
    }

    void zeroTimeout()
    {
        timeout = 0;
    }

    void setAI(movement newAI, bool switchDir)
    {
        ai = newAI;
        rev_flag = switchDir;
        if (newAI == FRIGHTENED)
            setSpeed(50);
        else if (newAI == DEAD)
        {
            setSpeed(200); // DEAD ghosts move at 200% speed, racing back to the SPAWN pen
            display_score = true;
        }
    }

    // prevent error 
    void roundPosition()
    {
        x = round(x / d_pos) * d_pos;
        y = round(y / d_pos) * d_pos;
    }

    void setSpeed(float percentage)
    {
        d_pos = percentage / 1000;
        roundPosition();
    }

    // set speed in percentqge 
    void aiSpawn()
    {
        setSpeed(50);
        if ((int)(y * 10.0f) % 10 == 5 && (int)(x * 10.0f) % 10 == 5 
            && canPass(getNextTile(dir)))
        {
            switch(dir)
            {
                case UP:
                    dir = DOWN;
                    break;
                case DOWN:
                    dir = UP;
                    break;
            }
        }
    }

    void aiLeave()
    {
        if (y < 19 && dir != DOWN)
        {
            setSpeed(50);  
            if (x < 13.4)    
                dir = RIGHT;
            else if (x > 13.6)
                dir = LEFT;
            else
            {
                x = 13.5;   
                dir = UP;   
            }
        }
        else if (y >= 19)    
        {
            dir = LEFT;     
            ai = wave;      
            setSpeed(100);  // Ensure speed is correctly set to 100%
        }
        else if ((int)(y * 10.0f) % 10 == 5 && canPass(getNextTile(dir)))
            dir = UP;
    }


    float distanceBetween(vector<int> p1, vector<int> p2)
    {
        float distancex = p1[0] - p2[0];
        float distancey = p1[1] - p2[1];
        return sqrt((distancex * distancex) + (distancey * distancey));
    }

    void help_target(vector<int>* npos, float* distance, direction* ndir, vector<int>* t){
        if (dir != LEFT && !canPass(getNextTile(RIGHT)))
        {
            *npos = {getX() + 1, getY()};
            float d = distanceBetween(*npos, *t);
            if (d < *distance)
            {
                *distance = d;
                *ndir = RIGHT;
            }
        }
        if (dir != UP && !canPass(getNextTile(DOWN)))
        {
            *npos = {getX(), getY() - 1};
            float d = distanceBetween(*npos, *t);
            if (d < *distance)
            {
                *distance = d;
                *ndir = DOWN;
            }
        }
        if (dir != RIGHT && !canPass(getNextTile(LEFT)))
        {
            *npos = {getX() - 1, getY()};
            float d = distanceBetween(*npos, *t);
            if (d < *distance)
            {
                *distance = d;
                *ndir = LEFT;
            }
        }
    }

    direction targetTile(vector<int> t)
    {
        vector<int> npos;
        float distance = 999; 
        direction ndir;
        if (!(getY() == 19 && (getX() == 12 || getX() == 15)) 
            && !(getY() == 7 && (getX() == 12 || getX() == 15)))
        {
            if (dir != DOWN && !canPass(getNextTile(UP)))
            {
                npos = {getX(), getY() + 1};
                float d = distanceBetween(npos, t);
                if (d < distance)
                {
                    distance = d;
                    ndir = UP;
                }
            }
        }
        help_target(&npos, &distance, &ndir, &t);
        return ndir;
    }


    void aiScatter()
    {
        vector<int> cible;
        switch(skin)
        {
            case RED:
                cible = {25, 33};
                break;
            case PINK:
                cible = {2, 33};
                break;
            case BLUE:
                cible = {27, -2};
                break;
            case YELLOW:
                cible = {0, -2};
                break;
        }
        dir = targetTile(cible);
        setSpeed(100);
    }

    vector<int> targetPacmanOffsetBy(int offsetSize)
    {
        vector<int> offset = {pacman.getX(), pacman.getY()};
        switch(pacman.getDirection())
        {
            case UP:
                offset[1] += offsetSize;
                break;
            case RIGHT:
                offset[0] += offsetSize;
                break;
            case DOWN:
                offset[1] -= offsetSize;
                break;
            case LEFT:
                offset[0] -= offsetSize;
                break;
        }
        return offset;
    }



    void aiChase(Ghost redGhost)
    {
        vector<int> cible = {pacman.getX(), pacman.getY()};   
        vector<int> current_pos = {getX(), getY()};            
        int distancex;    
        int distancey;    

        switch(skin)
        {
            case PINK:                              
                cible = targetPacmanOffsetBy(4);
                break;
            case BLUE:
                cible = targetPacmanOffsetBy(2);   // Start by finding the point 2 tiles ahead of Pac-Man in his direction of movement
                distancex = cible[0] - redGhost.getX();
                distancey = cible[1] - redGhost.getY();
                cible = {redGhost.getX() + 2 * distancex, redGhost.getY() + 2 * distancey};
                break;
            case YELLOW:
                if (distanceBetween(current_pos, cible) <= 8)
                    cible = {0, -2};
                break;
        }
        dir = targetTile(cible);
        setSpeed(100);
    }

    void aiFrightened()
    {
        direction newDir;
        do
        {
            newDir = static_cast<direction>((rand() % LEFT) + 1);
        } while (canPass(getNextTile(newDir)));

        dir = newDir;
        setSpeed(40);
    }

    void aiDead()
    {
        vector<int> cible = {14, 19};
        dir = targetTile(cible);
        setSpeed(200);
    }


    void checkSpecialCases()
    {
        if (timeout >= 600)      
        {
            if (ai == FRIGHTENED)
            {
                ai = wave;      
                setSpeed(100);  
            }
            timeout = -1;       
            ghostsEaten = 0;    
        }
        else if (timeout != -1) 
            timeout++;
        if (ai == DEAD)
        {
            if (x >= 13.4 && x <= 13.6)  
            {
                if (getY() == 19)   
                {
                    x = 13.5;       
                    dir = DOWN;     
                    setSpeed(50);
                }
                else if (getY() < 17 && getY() >= 15)
                    ai = LEAVE;     
            }
        }
    }


    void update_direction(Ghost redGhost){
        switch(ai)
        {
            case SCATTER:       
                aiScatter();
                break;
            case CHASE:         
                aiChase(redGhost);
                break;
            case FRIGHTENED:    
                aiFrightened();
                break;
            case DEAD:
                aiDead();
                break;
        }
    }

    void smooth_direction(){
        switch(dir)
        {
            case UP:
                y += d_pos;
                if (ai != SPAWN && ai != LEAVE && ai != DEAD)
                    x = round(x);
                break;
            case RIGHT:
                x += d_pos;
                if (ai != LEAVE)
                    y = round(y);
                break;
            case DOWN:
                y -= d_pos;
                if (ai != SPAWN && ai != LEAVE && ai != DEAD)
                    x = round(x);
                break;
            case LEFT:
                x -= d_pos;
                if (ai != LEAVE)
                    y = round(y);
                break;
        }
    }

    void move(Ghost redGhost)
    {
        checkSpecialCases();

        if (ai == SPAWN)
            aiSpawn();
        else if (ai == LEAVE)
            aiLeave();
        else if (isAtCenter() && getTile(getX(),getY()) == P)
        {
            if (dir == RIGHT)
                x = 1;
            else
                x = 26;
        }
        // If the a new AI mode has been set, rev_flag the current direction
        else if (rev_flag)
            rev_flagDirection();
        // If no special case exists, direction can only be changed when 2 or more exits exist
        else if (countExits() == 2 && canPass(getNextTile(dir)))
            turnCorner();
        else if (countExits() > 2)
            update_direction(redGhost);
        // Half speed when travelling down PORTAL corridors
        if (getY() == 16 && (getX() < 6 || getX() > 21) && ai != DEAD)
            setSpeed(50);
        smooth_direction();
        
    }

    unsigned int choose_eyes(){
        unsigned int eye;
        switch(dir)
        {
            case UP:
                eye = eye_u_tex;
                break;
            case RIGHT:
                eye = eye_r_tex;
                break;
            case DOWN:
                eye = eye_d_tex;
                break;
            case LEFT:
                eye = eye_l_tex;
                break;
        }
        return eye;
    }

    unsigned int get_ghost_texture(int* ga)
    {
        unsigned int ghost;
        if (ai == FRIGHTENED)
        {
            if (timeout >= 480 && tex_count % 30 >= 15)
                ghost = ghost_f_tex[*ga + 2];
            else
                ghost = ghost_f_tex[*ga];
        }
        else if (ai != DEAD)
        {
            switch(skin)
            {
                case RED:
                    ghost = ghost_r_tex[*ga];
                    break;
                case PINK:
                    ghost = ghost_p_tex[*ga];
                    break;
                case BLUE:
                    ghost = ghost_b_tex[*ga];
                    break;
                case YELLOW:
                    ghost = ghost_y_tex[*ga];
                    break;
            }
        }
        return ghost;
    }

    void draw()
    {
        display_score = false;
        prepare_rendering();
        glTranslatef(-3.0f, -3.0f, 0.0f);
        unsigned int ghost;
        int ga;

        ga = floor(tex_count % 20 / 10);
        ghost = get_ghost_texture(&ga);
        if (ai != DEAD)
            drawSprite(ghost, 14, 14, 0);
        if (ai != FRIGHTENED)
            drawSprite(choose_eyes(), 14, 14, 0);
        tex_count++;
        glPopMatrix();
    }

    void drawEaten()
    {
        if (display_score)
        {
            prepare_rendering();
            glTranslatef(-4.0f, 0.0f, 0.0f);
            int ghostScore = min(ghostsEaten - 1, 3);
            drawSprite(g_scores_tex[ghostScore], 16, 8, 0);
            glPopMatrix();
        }
        else
            draw();
    }

    void prepare_rendering(){
        glPushMatrix();
        translateMapOrigin();
        translateMapCoords(x, y);
    }
};

Ghost ghosts[4] = {
        Ghost(13.5, 19, RED),
        Ghost(13.5, 16, PINK),
        Ghost(11.5, 16, BLUE),
        Ghost(15.5, 16, YELLOW)
    };

void update_ghost_ai(){
    // Update ghost AI to new wave only if they are in CHASE/SCATTER mode
    // Checked every frame to ensure ghost AI correctly reset following FRIGHTENED mode
    for (int i = 0; i < 4; i++)
    {
        movement mv = ghosts[i].getAI();
        if ((mv == SCATTER || mv == CHASE) && mv != wave)
            ghosts[i].setAI(wave, true);
    }
}

void aiWave()
{
    int wave1, wave2, wave3, wave4, wave5, wave6, wave7;
    int playTicks = ticks - 240;
    // SCATTER: 7s, or 5s if level 2+
    if (level >= 5)
        wave1 = 5 * FRAME_RATE;
    else
        wave1 = 7 * FRAME_RATE;
    // CHASE: 20s
    wave2 = wave1 + 20 * FRAME_RATE;
    // SCATTER: 7s, or 5s if level 2+
    if (level >= 5)
        wave3 = wave2 + 5 * FRAME_RATE;
    else
        wave3 = wave2 + 7 * FRAME_RATE;
    wave4 = wave3 + 20 * FRAME_RATE;
    wave5 = wave4 + 5 * FRAME_RATE;
    if (level >= 5)
        wave6 = wave5 + 1037 * FRAME_RATE;
    else if (level >= 2)
        wave6 = wave5 + 1033 * FRAME_RATE;
    else
        wave6 = wave5 + 20 * FRAME_RATE;
    if (level >= 2)
        wave7 = wave6 + 1;
    else
        wave7 = wave6 + 5 * FRAME_RATE;
    if (playTicks <= wave1)
        wave = SCATTER;
    else if (playTicks <= wave2)
        wave = CHASE;
    else if (playTicks <= wave3)
        wave = SCATTER;
    else if (playTicks <= wave4)
        wave = CHASE;
    else if (playTicks <= wave5)
        wave = SCATTER;
    else if (playTicks <= wave6)
        wave = CHASE;
    else if (playTicks <= wave7)
        wave = SCATTER;
    else
        wave = CHASE;
    update_ghost_ai();
}

#endif /* !GHOSTS_H */
