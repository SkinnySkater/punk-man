#ifndef HELPER_H
    #define HELPER_H

#define FPS 30
#define ESCAPE_KEY 27
#define WAITING_TIME 50
#define BREAK_TIME 90
#define TOTAL_BALLS 244




void Handle_key(int key){
    switch (key)
    {
        case GLUT_KEY_UP:
            pacman.setDirection(UP);
            break;
        case GLUT_KEY_RIGHT:
            pacman.setDirection(RIGHT);
            break;
        case GLUT_KEY_DOWN:
            pacman.setDirection(DOWN);
            break;
        case GLUT_KEY_LEFT:
            pacman.setDirection(LEFT);
            break;
    }
}

void managePlayMode()
{
    if (timestamp == -1)
    {
        checkCollisions();
        pacman.move();
        checkCollisions();
        aiWave();
        for (int i = 0; i < 4; i++)
            ghosts[i].move(ghosts[0]);
        if (!fruitSpawned && fruits < level && pillsLeft <= 210 && rand() % 1500 == 0)
            addRandomFruit();
    }
    else
    {
        if (ticks == timestamp + BREAK_TIME)
        {
            // WIN !
            if (pillsLeft == 0)
            {
                pillsLeft = TOTAL_BALLS;
                level++;
                resetMap();
                resetLevel();
            }
            else
            {
                timestamp = ticks;
                mode = DEATH;
            }
        }
    }
}

void manageDeath(){
    if (ticks > timestamp + WAITING_TIME)
    {
        if (lives == 0)
        {
            mode = GAMEOVER;
            if (score > highscore)
            {
                highscore = score;
                setHighscore();
            }
        }
        else
        {
            lives--;
            resetFruit();
            resetLevel();
        }
    }
}

void displayPause(){
    bool isOver = tempMode == GAMEOVER;
    drawPause(isOver);
    drawLevel();
    drawScore();
    display_live();
    drawFruits();
    drawQuit();
}

#endif /* !HELPER_H */