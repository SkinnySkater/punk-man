#ifndef TYPES_H
	#define YPES_H
/**
 *W: Wall
 *G: Gate
 *P: Portal
 *n: Non-Filled Path
 *o: Pill
 *e: Eaten Pill
 *O: Big Pill
 *E: Eaten Big Pill
 *F: Fruit
 */
typedef enum {CHASE, SCATTER, FRIGHTENED, DEAD, LEAVE, SPAWN} movement;

typedef enum {READY, PLAY, FRUIT, EAT, PAUSE, DEATH, GAMEOVER} gamemode;

typedef enum {W, G, P, n, o, e, O, E, F} tile;

typedef enum {NONE, UP, RIGHT, DOWN, LEFT} direction;

typedef enum {RED, PINK, BLUE, YELLOW} color;

#endif /* !YPES_H */
