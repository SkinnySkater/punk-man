#ifndef GLOBALS_H
    #define GLOBALS_H

#define GHOSTS 4
#define EXTRA_LIFE_POINTS 10000
#define MAX_BALLS 244
#define BIGPILLS 50

int ticks = 0;
int timestamp = -1;

gamemode mode = READY;
// Save game mode when pausing the game
gamemode tempMode;

int score = 0;
int level = 1;
int lives = 2;
bool extraLife = false; // True if received
int pillsLeft = MAX_BALLS;
int fruits = 0;
bool fruitSpawned = false;

int ghostsEaten = 0;


void resetLevel()
{
    ticks = 0;
    timestamp = -1;
    pacman.reset();
    wave = SCATTER;
    ghostsEaten = 0;
    fruitSpawned = false;
    for (int i = 0; i < GHOSTS; i++)
        ghosts[i].reset();
    mode = READY;
}


void restartGame()
{
    score = 0;
    level = 1;
    lives = 2;
    extraLife = false;
    pillsLeft = MAX_BALLS;
    fruits = 0;
    resetMap();
    resetLevel();
}

void checkGhostsCollisions(){
    for (int i = 0; i < GHOSTS; i++)
    {
        if (ghosts[i].getX() == pacman.getX() && ghosts[i].getY() == pacman.getY())
        {
            if (ghosts[i].getAI() == wave)   // If the ghost is alive and not FRIGHTENED, Pac-Man will die
            {
                timestamp = ticks;
                pacman.stopFlashing();
                break;
            }
            else if (ghosts[i].getAI() == FRIGHTENED)    // If ghost is FRIGHTENED, it can be eaten itself
            {                                           // Set ghost AI to DEAD, increasing the score and count of ghosts eaten since the last big pill
                ghosts[i].setAI(DEAD, false);            // Briefly pause the game to show score for eating ghost
                score += 200 * pow(2, min(ghostsEaten++, 3));
                timestamp = ticks;
                pacman.stopFlashing();
                mode = EAT;
            }
        }
    }
}

void collision_helper(){
    if (pillsLeft == 0)
    {
        timestamp = ticks;
        pacman.stopFlashing();
    }
    if (!extraLife && score > EXTRA_LIFE_POINTS)
    {
        lives++;
        extraLife = true;
    }
}

void checkCollisions()
{
    // Eat current tile, increment score
    int scoreIncrement = pacman.eat();
    score += scoreIncrement;
    // set ghosts to FRIGHTENED mODE
    if (scoreIncrement == BIGPILLS)
    {
        for (int i = 0; i < GHOSTS; i++)
        {
            ghosts[i].zeroTimeout();
            if (ghosts[i].getAI() == wave || ghosts[i].getAI() == FRIGHTENED)
                ghosts[i].setAI(FRIGHTENED, true);
        }
    }
    else if (scoreIncrement >= 100)
    {
        timestamp = ticks;
        pacman.stopFlashing();
        mode = FRUIT;
    }
    collision_helper();
    if (ghosts[2].getAI() == SPAWN && pillsLeft <= MAX_BALLS - 30 && ticks >= 300)
        ghosts[2].setAI(LEAVE, false);
    else if (ghosts[3].getAI() == SPAWN && pillsLeft <= MAX_BALLS * 2 / 3 && ticks >= 420)
        ghosts[3].setAI(LEAVE, false);
    checkGhostsCollisions();
}


void drawPlayScreen()
{
    drawMap();
    drawLevel();
    drawScore();
    display_live();
    drawFruits();
    drawHelp();
}

void drawCharacters()
{
    pacman.draw();
    for (int i = 0; i < GHOSTS; i++)
        ghosts[i].draw();
}

#endif /* !GLOBALS_H */
