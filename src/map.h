
#ifndef PACMAN_MAP_H
#define PACMAN_MAP_H

#define TILEs_SIZE 8
#define SPRITE_SIZE 14

extern int ticks;
extern int fruits;
extern bool fruitSpawned;

int fruitTimer = -1;

tile map[28][31] =
    {
        {W,W,W,W,W,W,W,W,W,W,W,W,n,n,n,W,P,W,n,n,n,W,W,W,W,W,W,W,W,W,W},
        {W,o,o,o,o,W,W,O,o,o,o,W,n,n,n,W,n,W,n,n,n,W,o,o,o,o,O,o,o,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,W,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,W,W,W,o,W,W,W,W,W,n,W,W,W,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,W},
        {W,o,W,W,W,W,W,o,W,W,o,W,W,W,W,W,n,W,W,W,W,W,W,W,W,o,W,W,W,o,W},
        {W,o,W,W,W,W,W,o,W,W,o,W,W,W,W,W,n,W,W,W,W,W,W,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,W,W,o,n,n,n,n,n,n,n,n,n,W,W,o,o,o,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,W,n,W,W,W,W,W,n,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,W,n,W,W,n,n,W,n,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,o,o,o,W,W,o,o,o,o,W,W,n,W,W,n,n,W,n,n,n,o,W,W,o,o,o,o,o,W},
        {W,o,W,W,W,W,W,n,W,W,W,W,W,n,W,W,n,n,G,n,W,W,W,W,W,o,W,W,W,W,W},
        {W,o,W,W,W,W,W,n,W,W,W,W,W,n,W,W,n,n,G,n,W,W,W,W,W,o,W,W,W,W,W},
        {W,o,o,o,o,W,W,o,o,o,o,W,W,n,W,W,n,n,W,n,n,n,o,W,W,o,o,o,o,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,W,n,W,W,n,n,W,n,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,W,n,W,W,W,W,W,n,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,W,W,o,n,n,n,n,n,n,n,n,n,W,W,o,o,o,o,W,W,W,o,W},
        {W,o,W,W,W,W,W,o,W,W,o,W,W,W,W,W,n,W,W,W,W,W,W,W,W,o,W,W,W,o,W},
        {W,o,W,W,W,W,W,o,W,W,o,W,W,W,W,W,n,W,W,W,W,W,W,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,W},
        {W,o,W,W,o,W,W,W,W,W,o,W,W,W,W,W,n,W,W,W,W,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,W,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,o,o,o,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,W,W,o,W,W,o,W,W,o,W,n,n,n,W,n,W,n,n,n,W,o,W,W,o,W,W,W,o,W},
        {W,o,o,o,o,W,W,O,o,o,o,W,n,n,n,W,n,W,n,n,n,W,o,o,o,o,O,o,o,o,W},
        {W,W,W,W,W,W,W,W,W,W,W,W,n,n,n,W,P,W,n,n,n,W,W,W,W,W,W,W,W,W,W}
    };


void translateMapOrigin()
{
    glTranslatef (38.0f, 26.0f, 0.0f);
}


void translateMapCoords(float x, float y)
{
    glTranslatef (x * TILEs_SIZE, y * TILEs_SIZE, 0.0f);
}

tile getTile(int x, int y)
{
    return map[x][y];
}


void setTile(int x, int y, tile t)
{
    map[x][y] = t;
}


bool canPass(tile t)
{
    return t == W || t == G;
}


void resetMap()
{
    for (int x = 0; x < 28; x++)
    {
        for (int y = 0 ; y < 31; y++)
        {
            switch(getTile(x, y))
            {
                case F:
                case e:
                    setTile(x, y, o);
                    break;
                case E:
                    setTile(x, y, O);
                    break;
            }
        }
    }
}

// call it when player dies
void resetFruit()
{
    for (int x = 0; x < 28; x++)
    {
        for (int y = 0; y < 31; y++)
        {
            switch(getTile(x,y))
            {
                case F:
                    setTile(x, y, e);
                    break;
            }
        }
    }
}


void addRandomFruit()
{
    int x;
    int y;
    do
    {
        x = rand() % 27 + 1;
        y = rand() % 10 + 1;
    } while (getTile(x, y) != e);
    setTile(x, y, F);
    fruitSpawned = true;
    fruitTimer = 0;
}

void drawFruit(int x, int y)
{
    if (fruitTimer <= 900)
    {
        glPushMatrix();
        glTranslatef (-3.0f, -3.0f, 0.0f);
        drawSprite(fruits_tex[fruits], 14, 14, 0);
        glPopMatrix();
        fruitTimer++;
    }
    else
    {
        setTile(x, y, e);
        fruitTimer = -1;
    }
}


void drawMap()
{
    glPushMatrix();
    translateMapOrigin();
    drawSprite(map_tex, 224, 248, 0);
    for (int x = 0; x < 28; x++)
    {
        glPushMatrix();
        for (int y = 0; y < 31; y++)
        {
            int bp = floor(ticks % 40 / 20);
            switch (getTile(x, y))
            {
                case o:
                    drawSprite(pill_tex, TILEs_SIZE, TILEs_SIZE, 0);
                    break;
                case O:
                    drawSprite(bigPill_tex[bp], TILEs_SIZE, TILEs_SIZE, 0);
                    break;
                case F:
                    drawFruit(x, y);
                    break;
            }
            translateMapCoords(0, 1);
        }
        glPopMatrix();
        translateMapCoords(1, 0);
    }
    glPopMatrix();
}
 
#endif /* !MAP_H */
