#include <GL/glut.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <iostream>
#include <png.h>
#include <vector>
#include <fstream>
#include <chrono>
#include <unistd.h>

using namespace std;
using namespace std::chrono;

#include "png_load.h"
#include "load.h"

#include "types.h"
#include "textures.h"
#include "map.h"
#include "ui.h"
#include "pacman.h"
#include "ghosts.h"
#include "globals.h"
#include "helper.h"

#define FPS 30
#define ESCAPE_KEY 27
#define WAITING_TIME 50
#define BREAK_TIME 90
#define TOTAL_BALLS 244

float frameLength = 1000 / FPS;
milliseconds last;
milliseconds now;

void drawBitmapText(char *string,float x,float y,float z) 
{  
    char *c;
    glRasterPos3f(x, y,z);

    for (c=string; *c; c++) 
    {
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_10, *c);
    }
}


void loop()
{
    now = duration_cast<milliseconds>(system_clock::now().time_since_epoch());

    
    float fp = float(now.count() - last.count());
    string stfps = to_string(fp);
    drawBitmapText((char *)stfps.c_str() ,0,10, 0);
    glutSwapBuffers();
    last = now;
    switch(mode)
    {
        case READY:
            if (ticks > WAITING_TIME)
                mode = PLAY;
            break;
        case PLAY:
            // If timestamp is not set, execute all PLAY-mode logic
            managePlayMode();
            break;
        case FRUIT:
        case EAT:
            if (ticks == timestamp + 20)
            {
                timestamp = -1;
                pacman.startFlashing();
                mode = PLAY;
            }
            break;
        case DEATH:
            manageDeath();
            break;
    }
    glutPostRedisplay();
    // Increment game ticks once the frame is drawn, but only if not paused
    if (mode != PAUSE)
        ticks++;
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    switch(mode)
    {
        case READY:
            drawPlayScreen();
            drawCharacters();
            drawReady();
            break;
        case PLAY:
            drawPlayScreen();
            drawCharacters();
            break;
        case FRUIT:
            drawPlayScreen();
            for(int i = 0; i < 4; i++)
                ghosts[i].draw();
            pacman.drawFruitScore();
            break;
        case EAT:
            drawPlayScreen();
            for (int i = 0; i < 4; i++)
                ghosts[i].drawEaten();
            break;
        case PAUSE:
            displayPause();
            break;
        case DEATH:
            drawPlayScreen();
            pacman.drawDead();
            break;
        case GAMEOVER:
            drawPlayScreen();
            drawGameover();
            break;
    }
    glutSwapBuffers();
}


void handle_user_input(unsigned char key, int, int) {
    switch (key) {
        case ESCAPE_KEY:
            if (mode != PAUSE)
            {
                tempMode = mode;
                mode = PAUSE;
            }
            else if (mode == PAUSE)
                exit(1);
            break;
        default:
            if (mode == PAUSE && tempMode != GAMEOVER)
                mode = tempMode;
            else if (mode == GAMEOVER || mode == PAUSE)
                restartGame();
            break;
    }
}



void handle_sp_event(int key, int, int)
{
    if (mode == EAT || mode == PLAY || mode == READY)
        Handle_key(key);
    else
    {
        switch (key)
        {
            default:
                if (mode == PAUSE && tempMode != GAMEOVER)
                    mode = tempMode;
                else if (mode == GAMEOVER || mode == PAUSE)
                    restartGame();
                break;
        }
    }
}

void on_tap(int vis)
{
    if (vis == GLUT_VISIBLE)
        glutIdleFunc(loop);
    else
        glutIdleFunc(NULL);
}

void initBackgroundColor(){
    float r = 0 + static_cast <float> (rand()) / ( static_cast <float> (RAND_MAX/(254 - 0)));
    float g = 0 + static_cast <float> (rand()) / ( static_cast <float> (RAND_MAX/(254 - 0)));
    float b = 0 + static_cast <float> (rand()) / ( static_cast <float> (RAND_MAX/(254 - 0)));
    glClearColor(r, g, b, 0.0f);
}

void init()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, 300, 0, 300);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    loadBindTextures();
    getHighscore();
    last = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
}

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(650, 650);
    glutInitWindowPosition(550, 350);
    glutCreateWindow("Punk-Man");
    glutDisplayFunc(display);
    glutKeyboardFunc(handle_user_input);
    glutSpecialFunc(handle_sp_event);
    glutVisibilityFunc(on_tap);
    init();
    glutMainLoop();
    return 0;
}
