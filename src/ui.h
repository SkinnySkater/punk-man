#ifndef PACMAN_UI_H
    #define PACMAN_UI_H

#define DATA_FILE "data.txt"

extern int score;
extern int level;
extern int lives;
extern int fruits;

int highscore;

void init_render(){
    glPushMatrix();
    translateMapOrigin(); 
}


void drawReady()
{
    init_render();
    translateMapCoords(11, 13);
    drawSprite(ready_tex, 48, 8, 0);
    glPopMatrix();
}

void drawGameover()
{
    init_render();
    translateMapCoords(9, 13);
    drawSprite(gameover_tex, 80, 8, 0);
    glPopMatrix();
}

void drawNumberAsSprite(int number)
{
    glPushMatrix();
    string str = to_string(number);
    for (int i = str.length() - 1; i >= 0; i--)
    {
        switch(str[i])
        {
            case '0':
                drawSprite(num_0_tex, 8, 8, 0);   
                break;
            case '1':
                drawSprite(num_1_tex, 8, 8, 0);     
                break;
            case '2':
                drawSprite(num_2_tex, 8, 8, 0);     
                break;
            case '3':
                drawSprite(num_3_tex, 8, 8, 0);     
                break;
            case '4':
                drawSprite(num_4_tex, 8, 8, 0);     
                break;
            case '5':
                drawSprite(num_5_tex, 8, 8, 0);     
                break;
            case '6':
                drawSprite(num_6_tex, 8, 8, 0);     
                break;
            case '7':
                drawSprite(num_7_tex, 8, 8, 0);     
                break;
            case '8':
                drawSprite(num_8_tex, 8, 8, 0);     
                break;
            case '9':
                drawSprite(num_9_tex, 8, 8, 0);     
                break;
        }
        translateMapCoords(-1, 0);
    }
    if (str.length() == 1)
        drawSprite(num_0_tex, 8, 8, 0);
    glPopMatrix();
}

void drawScore()
{
    init_render();              
    translateMapCoords(6.5, 32.5);       
    drawSprite(score_tex, 80, 8, 0);    
    translateMapCoords(4, -1);           
    drawNumberAsSprite(min(highscore, 99999));   
    translateMapCoords(6, 0);            
    drawNumberAsSprite(min(score, 99999));   

    glPopMatrix();
}

void drawLevel()
{
    init_render();              
    translateMapCoords(1, 32.5);         
    drawSprite(one_up_tex, 24, 8, 0);   
    translateMapCoords(3, -1);           
    drawNumberAsSprite(level);          
    glPopMatrix();
}

void display_live()
{
    init_render();                  
    translateMapCoords(1, -2.5);
    for(int i = 0; i < lives; i++)
    {
        drawSprite(life_tex, 14, 14, 0);
        translateMapCoords(2, 0);
    }
    glPopMatrix();
}

void drawHelp()
{
    init_render();
    translateMapCoords(19, 32);
    drawSprite(help_tex, 64, 8, 0);
    glPopMatrix();
}

void drawPause(bool gameover)
{
    init_render();
    if (!gameover)
        drawSprite(pause_tex, 224, 248, 0);
    else
        drawSprite(pause_alt_tex, 224, 248, 0);
    glPopMatrix();
}

void drawQuit()
{
    init_render();              
    translateMapCoords(19,32);          
    drawSprite(quit_tex, 64, 8, 0);     
    glPopMatrix();
}


void getHighscore()
{
    fstream file(DATA_FILE);
    if (file.good())
        file >> highscore;
    else
    {
        ofstream newFile(DATA_FILE);
        newFile << 0;
        newFile.close();
    }
    file.close();
}

void setHighscore()
{
    ofstream file(DATA_FILE);
    file.clear();
    file << highscore;
    file.close();
}


void drawFruits()
{
    init_render();                      
    translateMapCoords(25, -2.5);                
    for (int i = 0; i < fruits; i++)
    {
        drawSprite(fruits_tex[i], 14, 14, 0);   
        translateMapCoords(-2,0);              
    }

    glPopMatrix();
}

#endif /* !PACMAN_UI_H */
