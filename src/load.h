#ifndef LOAD_AND_BIND_TEXTURE_H
#define LOAD_AND_BIND_TEXTURE_H


unsigned int load_exture(const char* filename)
{
  char* image_buffer = NULL;
	int w = 0;
	int h = 0;
  unsigned int tflag = 0;

	if (!load_png_sprite(filename, &w, &h, &image_buffer))
  {
      fprintf(stderr, "textureForm file doesn't exist: %s\n", filename);
      exit(1);
  }
	glGenTextures(1, &tflag); 
	glBindTexture(GL_TEXTURE_2D, tflag);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)image_buffer);
  glBindTexture(GL_TEXTURE_2D, 0);
	free(image_buffer);

	return tflag;
}

#endif /* !LOAD_AND_BIND_TEXTURE_H */
