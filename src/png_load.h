#ifndef PNGLOAD_H
    #define PNGLOAD_H

#define BYTE 8
//TODO: Handle errors

int error_png(FILE* fp, const char* msg, string fname){
    fprintf(stderr, msg, fname);
    fclose(fp);
    return 0;
}


int load_png_sprite(const char* fname, int* w, int* h, char** data_p_png)
{
    int rb = 0;
    int i;
    FILE* fp = fopen(fname, "rb");
    png_byte header[BYTE];
    // row_pointers is for pointing to png_data for reading the png with libpng
    png_bytep* row_pointers = NULL;
    png_byte* png_data = NULL;
    png_structp p_png = NULL;
    png_infop info_ptr = NULL;
    png_infop end_info = NULL;
    int depth, color_type;
    png_uint_32 tw, th;

    fread(header, 1, BYTE, fp);
    p_png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(p_png);
    end_info = png_create_info_struct(p_png);
    
    // init png reading
    png_init_io(p_png, fp);
    // let libpng know you already read the first 8 bytes
    png_set_sig_bytes(p_png, BYTE);
    // read all the info up to the image data
    png_read_info(p_png, info_ptr);
    // get info about png
    png_get_IHDR(p_png, info_ptr, &tw, &th, &depth, &color_type, NULL, NULL, NULL);

    if (w)
        *w = tw;
    if (h)
        *h = th;
    png_read_update_info(p_png, info_ptr);
    rb = png_get_rowbytes(p_png, info_ptr);
    // glTexImage2d requires rows to be 4-byte aligned
    rb += 3 - ((rb - 1) % BYTE / 2);
    // Allocate the png_data as a big block, to be given to opengl
    png_data = (png_byte*) malloc(rb * th * sizeof(png_byte) + 15);
    
    row_pointers = (png_bytep*) malloc(th * sizeof(png_bytep));
    for (i = 0; i < th; i++)
        row_pointers[th - 1 - i] = png_data + i * rb;
    // read the png into png_data through row_pointers
    png_read_image(p_png, row_pointers);
    png_destroy_read_struct(&p_png, &info_ptr, &end_info);
	*data_p_png = (char*) png_data;

    free(row_pointers);
    fclose(fp);
	return 1;
}

#endif /* !PNGLOAD_H */
