CXXFLAGS 	= -O3 -std=c++14 -I/modules/cs324/glew-1.11.0/include

LDLIBS 		= -lglut -lGL -lGLU  -lm -lpng -lX11

LDFLAGS 	= -L/usr/X11R6/lib -L/modules/cs324/glew-1.11.0/lib -Wl,-rpath=/modules/cs324/glew-1.11.0/lib

.PHONY: clean

EXE 		= pacman

SRCS 		= src/pacman.cpp

OBJS 		=  $(SRCS:.cpp=.o)

CXX 		= g++

$(EXE):  $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ $(LDLIBS) -o $@

clean:
	-@rm $(OBJS) $(EXE)
